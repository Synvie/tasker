# SCTP

Semi Config Tasker Profiles

### Tasker Variable Repository Manager

**Local Variable Repository :**
1. Download and extract [Example Tasker Profiles](https://gitlab.com/Synvie/tasker/archive/main/tasker-main.zip)
2. Open Tasker on phone
3. Click "+" on TASKS Tab and rename `local variable repository`
4. Open `local variable repository`
5. Click "+", Search `Read File` and add this action
   - On `File`, Click "🔍", add your files to read
   - In this example case open `Downloads` > `parcel_tracking.csv`
   - On `To Var`, add a Variable
     - In this example rename `%PARCEL`
   - ✔ Structure Output (JSON,etc)
6. Enjoy create a powerful, creativity, innovative tasker profiles

**Remote Variable Repository :**
1. Open Tasker on phone
2. Click "+" on TASKS Tab and rename `Remote variable repository`
3. Open `Remote variable repository`
4. Click "+", Search `HTTP Request` and add this action
   - On `Method`, Select `GET`
   - On `URL`, Add a remote variable repository files URL
     - In this example add `https://gitlab.com/Synvie/tasker/raw/main/config/parcel_tracking.csv`
   - ✔ Structure Output (JSON,etc)
5. Click "+", Search `Variable Set` and add this action
   - On `Name`, add a Variable
     - In this example rename `%PARCEL`
   - On `To`, add `%http_data`
   - ✔ Structure Output (JSON,etc)
6. Enjoy create a powerful, creativity, innovative tasker profiles

